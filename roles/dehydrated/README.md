# Ansible Role: dehydrated

Installs and configures dehydrated as a service

## Dependencies: 

- Ansible Role: scc_net.base.apt_repository

## Variables:

Variables you might want to change:

```yaml
dehydrated_user: "dehydrated"
dehydrated_group: "{{ dehydrated_user }}"

dehydrated_contact_email: "net-server@lists.kit.edu"
dehydrated_config_dir: "/etc/dehydrated"
dehydrated_conf_d: "{{ dehydrated_config_dir }}/conf.d"
dehydrated_domains_d: "{{ dehydrated_config_dir }}/domains.d"
dehydrated_domains_txt: "{{ dehydrated_config_dir }}/domains.txt"
dehydrated_basedir: "/var/lib/dehydrated"
dehydrated_certdir: "{{ dehydrated_basedir }}/certs"
dehydrated_hook_script_path: "{{ dehydrated_config_dir }}/hook.sh"
```


#!/usr/bin/env python3
# DOC: https://github.com/dehydrated-io/dehydrated/blob/master/docs/examples/hook.sh

import argparse
import json
import os
import sys
import time
from urllib import request
import collections


class HostingDEAPIException(Exception):
    pass


class NoDataFoundException(HostingDEAPIException):
    pass


class HostingDEAPIErrorException(HostingDEAPIException):
    def __init__(self, message: str, errors: list):
        super().__init__(message)
        self.errors = errors


class HostingDEAPI:
    def __init__(self):
        api_key_file_path = os.path.abspath(os.path.expanduser('~/.hostingde.apikey'))
        with open(api_key_file_path, 'r') as apikey_file:
            api_key_file_content = [line.strip() for line in apikey_file if not line.strip().startswith('#') and line.strip() != '']

        if len(api_key_file_content) != 1:
            raise HostingDEAPIException('Your file containing the api-key looks weird. Only the API-Key in one line, blank lines and lines starting with "#" are allowed.')

        self.api_key = api_key_file_content[0]

    def post_data(self, url: str, data: dict) -> dict:
        """
        POST data string to `url`, returns response
        """
        req = request.Request(url)
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        data["authToken"] = self.api_key
        jsondata = json.dumps(data).encode('utf-8')
        resource = request.urlopen(req, jsondata)
        result_plain = resource.read().decode(resource.headers.get_content_charset())
        result = json.loads(result_plain)

        if result['status'] == 'error':
            linebreak = "\n"
            raise HostingDEAPIErrorException(f'API call failed with error:\n{linebreak.join(err["text"] for err in result["errors"])}', errors=result['errors'])

        return result

    def lookup_zone_config_by_id(self, zone_id: str) -> dict:
        data = {
            "filter": {
                        "field": "ZoneConfigId",
                        "value": zone_id
            },
            "limit": 1
        }
        content = self.post_data("https://secure.hosting.de/api/dns/v1/json/zoneConfigsFind", data)
        return content['response']['data']

    def lookup_zone_config_by_name(self, name: str) -> dict:
        data = {
            "filter": {
                "subFilterConnective": "OR",
                "subFilter": [
                    {
                        "field": "ZoneNameUnicode",
                        "value": name
                    },
                    {
                        "field": "ZoneName",
                        "value": name,
                    }
                ]
            },
            "limit": 1
        }
        content = self.post_data("https://secure.hosting.de/api/dns/v1/json/zoneConfigsFind", data)
        return content['response']['data']

    def get_zone_config(self, fqdn: str):
        fqdn_split = fqdn.split('.')
        zone_config = []
        for i in range(len(fqdn_split)-1):
            zone_config = self.lookup_zone_config_by_name('.'.join(fqdn_split[i:]))
            if len(zone_config) == 1:
                break

        if len(zone_config) == 0:
            raise NoDataFoundException(f'No zone for fqdn {fqdn} found')

        if zone_config[0]['type'] != 'NATIVE':
            raise HostingDEAPIException(f'Not native zones are not changeable via hosting.de API.')

        return zone_config[0]

    def check_zone_active(self, zone_id):
        zone_config = self.lookup_zone_config_by_id(zone_id)
        while zone_config[0].get('status', False) != 'active':
            time.sleep(5.0)
            zone_config = self.lookup_zone_config_by_id(zone_id)

    def add_txt_challenge_record(self, zone_config_id, fqdn, challenge):
        api_endpoint = "https://secure.hosting.de/api/dns/v1/json/recordsUpdate"
        data = {
            "zoneConfigId": zone_config_id,
            "recordsToAdd": [
                {
                    "name": f"_acme-challenge.{fqdn}",
                    "type": "TXT",
                    "content": f'"{challenge}"',
                    "ttl": 60
                }
            ]
        }
        try:
            content = self.post_data(api_endpoint, data)

            if content['status'] == 'pending':
                print('Check if changes are deployed')
                zone_id = content['response']['zoneConfig']['id']
                self.check_zone_active(zone_id)

            print(f'Challenge deployed for {fqdn}')
            return 0
        except HostingDEAPIErrorException as e:
            for err in e.errors:
                if err['code'] == 40010:
                    print(f'Challenge record for {fqdn} with token {challenge} does already exist!')
                    return 0
                else:
                    print(f'Unknown error occured: {err["text"]}')
                    return 1

    def find_challenge_record(self, fqdn, challenge):
        challenge_record = f'_acme-challenge.{fqdn}'
        data = {
            'filter': {
                'subFilterConnective': 'AND',
                'subFilter': [
                    {'field': 'RecordName', 'value': challenge_record},
                    {'field': 'RecordType', 'value': 'TXT'},
                    {'field': 'RecordContent', 'value': f'"{challenge}"'}
                ]
            }
        }

        content = self.post_data("https://secure.hosting.de/api/dns/v1/json/recordsFind", data)
        record_data = content['response']['data']

        if len(record_data) == 0:
            raise NoDataFoundException(f'No TXT record "{challenge_record}: {challenge}" found')

        if len(record_data) != 1:
            raise HostingDEAPIException(f'WUT? Found multiple records, this should be impossible.')

        return record_data[0]

    def delete_txt_record(self, zone_config_id, record_data):
        data = {
            "zoneConfigId": zone_config_id,
            "recordsToDelete": [record_data]
        }
        content = self.post_data('https://secure.hosting.de/api/dns/v1/json/recordsUpdate', data)

        if content['status'] == 'pending':
            print('Check if changes are deployed')
            zone_id = content['response']['zoneConfig']['id']
            self.check_zone_active(zone_id)

        print('Challenge deleted')
        return 0


def deploy_challenge(arguments):
    api = HostingDEAPI()

    try:
        zone_config = api.get_zone_config(arguments.domain)
    except HostingDEAPIException as e:
        print(e)
        sys.exit(1)
    zone_config_id = zone_config['id']

    status = api.add_txt_challenge_record(zone_config_id, arguments.domain, arguments.token)
    if status != 0:
        sys.exit(status)


def clean_challenge(arguments):
    api = HostingDEAPI()

    try:
        record_data = api.find_challenge_record(arguments.domain, arguments.token)
    except NoDataFoundException as e:
        print(e)
        sys.exit(0)
    except HostingDEAPIException as e:
        print(e)
        sys.exit(1)

    zone_config_id = record_data["zoneConfigId"]
    status = api.delete_txt_record(zone_config_id, record_data)
    if status != 0:
        sys.exit(status)


def not_implemented(arguments):
    sys.exit(0)


def parse_args():
    parser = argparse.ArgumentParser(description="Hosting.de dns api script for dehydrated")
    subparsers = parser.add_subparsers(help='sub-command help', dest='hook')

    # parse optional arguments with + instead of - because challenges can contain -
    parser_deploy_challenge = subparsers.add_parser('deploy_challenge', prefix_chars='+', help='make ACME challenge available via DNS')
    parser_deploy_challenge.set_defaults(func=deploy_challenge)
    parser_deploy_challenge.add_argument('domain', help="domain name to request certificate for")
    parser_deploy_challenge.add_argument('tokenfile', help="IGNORED")
    parser_deploy_challenge.add_argument('token', help="ACME-provided token")

    parser_clean_challenge = subparsers.add_parser('clean_challenge', prefix_chars='+', help='remove ACME challenge from DNS')
    parser_clean_challenge.set_defaults(func=clean_challenge)
    parser_clean_challenge.add_argument('domain', help="domain name for which to remove cetificate challenge")
    parser_clean_challenge.add_argument('tokenfile', help="IGNORED")
    parser_clean_challenge.add_argument('token', help="ACME-provided token")

    parser_sync_cert = subparsers.add_parser('sync_cert', prefix_chars='+')
    parser_sync_cert.set_defaults(func=not_implemented)
    parser_sync_cert.add_argument('keyfile')
    parser_sync_cert.add_argument('certfile')
    parser_sync_cert.add_argument('fullchainfile')
    parser_sync_cert.add_argument('chainfile')
    parser_sync_cert.add_argument('requestfile')

    parser_deploy_cert = subparsers.add_parser('deploy_cert', prefix_chars='+')
    parser_deploy_cert.set_defaults(func=not_implemented)
    parser_deploy_cert.add_argument('domain')
    parser_deploy_cert.add_argument('keyfile')
    parser_deploy_cert.add_argument('certfile')
    parser_deploy_cert.add_argument('fullchain')
    parser_deploy_cert.add_argument('chainfile')
    parser_deploy_cert.add_argument('timestamp')

    parser_deploy_ocsp = subparsers.add_parser('deploy_ocsp', prefix_chars='+')
    parser_deploy_ocsp.set_defaults(func=not_implemented)
    parser_deploy_ocsp.add_argument('domain')
    parser_deploy_ocsp.add_argument('ocspfile')
    parser_deploy_ocsp.add_argument('timestamp')

    parser_unchanged_cert = subparsers.add_parser('unchanged_cert', prefix_chars='+')
    parser_unchanged_cert.set_defaults(func=not_implemented)
    parser_unchanged_cert.add_argument('domain')
    parser_unchanged_cert.add_argument('keyfile')
    parser_unchanged_cert.add_argument('certfile')
    parser_unchanged_cert.add_argument('fullchainfile')
    parser_unchanged_cert.add_argument('chainfile')

    parser_invalid_challenge = subparsers.add_parser('invalid_challenge', prefix_chars='+')
    parser_invalid_challenge.set_defaults(func=not_implemented)
    parser_invalid_challenge.add_argument('domain')
    parser_invalid_challenge.add_argument('response')

    parser_request_failure = subparsers.add_parser('request_failure', prefix_chars='+')
    parser_request_failure.set_defaults(func=not_implemented)
    parser_request_failure.add_argument('statuscode')
    parser_request_failure.add_argument('reason')
    parser_request_failure.add_argument('reqtype')
    parser_request_failure.add_argument('headers')

    parser_generate_csr = subparsers.add_parser('generate_csr', prefix_chars='+')
    parser_generate_csr.set_defaults(func=not_implemented)
    parser_generate_csr.add_argument('domain')
    parser_generate_csr.add_argument('certdir')
    parser_generate_csr.add_argument('altname')

    parser_startup_hook = subparsers.add_parser('startup_hook', prefix_chars='+')
    parser_startup_hook.set_defaults(func=not_implemented)

    parser_exit_hook = subparsers.add_parser('exit_hook', prefix_chars='+')
    parser_exit_hook.set_defaults(func=not_implemented)

    # prepare to ignore unrecognised hooks
    fallback_parser = argparse.ArgumentParser()
    fallback_parser.add_argument('_hook_arg', nargs='*')
    subparsers._name_parser_map = collections.defaultdict(
        lambda: fallback_parser,
        subparsers._name_parser_map)
    subparsers.choices = None

    arguments = parser.parse_args()

    # ignore unrecognised hooks
    if not hasattr(arguments, 'func'):
        print("ignoring unknown hook: %s" % arguments.hook)
        return lambda x: None, arguments

    return arguments.func, arguments


if __name__ == "__main__":
    func, args = parse_args()
    func(args)

#!/usr/bin/python3
DOCUMENTATION = """
        action: mediawiki_upload_page
        short_description: uploads page to mediawiki instance via API
        description:
            - mediawiki upload plugin
        option:
          domain:
            description: mediawiki domain
            ini:
              - section: mediawiki
                key: domain
            required: True
            type: string
"""
from ansible.errors import AnsibleActionFail
from ansible.plugins.action import ActionBase
import requests
from ipaddress import IPv4Address, IPv6Address, ip_address


class HostingDeAPI:
    def __init__(self, apikey):
        self._payload_template = {'authToken': apikey}
        self.session = requests.session()
        self.session.headers.update({'Content-Type': 'application/json; charset=utf-8'})

    @property
    def user_id(self):
        req = self.session.post('https://secure.hosting.de/api/account/v1/json/getOwnUser', json=self._payload_template)
        return req.json()['response']['id']

    def create_acme_api_key(self, name: str, allowed_ips: list[IPv6Address, IPv4Address]):
        payload = self._payload_template
        payload['apiKey'] = {
            'allowedIps': [str(ip) for ip in allowed_ips],
            'name': name,
            'rights': ['DNS_ZONES_LIST', 'DNS_ZONES_EDIT'],
            'userId': self.user_id
        }
        req = self.session.post('https://secure.hosting.de/api/account/v1/json/apiKeyCreate', json=payload)
        apikey = req.json()['response']['key']
        return apikey


class ActionModule(ActionBase):
    # A set of valid arguments
    _VALID_ARGS = frozenset(('apikey', ))
    _REQUIRED_ARGS = _VALID_ARGS

    def run(self, tmp=None, task_vars=None):
        if task_vars is None:
            task_vars = dict()

        result = super(ActionModule, self).run(tmp, task_vars)
        del tmp  # tmp no longer has any effect

        # Error if invalid argument is passed
        if self._VALID_ARGS:
            task_opts = frozenset(self._task.args.keys())
            bad_opts = task_opts.difference(self._VALID_ARGS)
            if bad_opts:
                raise AnsibleActionFail(f'Invalid options for {self._task.action}: {",".join(list(bad_opts))}')

        # error if required arguments are missing
        if self._REQUIRED_ARGS:
            task_opts = frozenset(self._task.args.keys())
            missing_opts = self._REQUIRED_ARGS.difference(task_opts)
            if missing_opts:
                raise AnsibleActionFail(f'Missing options for {self._task.action}: "{",".join(list(missing_opts))}"')

        apikey = self._task.args.get('apikey')

        inventory_hostname = self._templar.template('{{inventory_hostname}}')

        ipv6_addresses = self._templar.template('{{ansible_facts.all_ipv6_addresses}}')
        ipv4_addresses = self._templar.template('{{ansible_facts.all_ipv4_addresses}}')
        global_ip_addresses = [ip_address(ip) for ip in ipv6_addresses + ipv4_addresses if ip_address(ip).is_global]

        api = HostingDeAPI(apikey)
        result['ansible_facts'] = {'__hostingde_apikey_new': api.create_acme_api_key(inventory_hostname, global_ip_addresses)}

        return result

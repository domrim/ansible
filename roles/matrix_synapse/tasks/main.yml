---
- name: Set OS dependent variables
  ansible.builtin.include_vars: "{{ lookup('first_found', params) }}"
  vars:
    params:
      files:
        - "{{ ansible_distribution | lower }}_{{ ansible_distribution_version | lower }}.yml"
        - "{{ ansible_distribution | lower }}_{{ ansible_distribution_major_version | lower }}.yml"
        - "{{ ansible_distribution | lower }}.yml"
        - "{{ ansible_os_family | lower }}.yml"
        - "{{ ansible_system | lower }}.yml"
      paths:
        - "{{ role_path }}/vars"
  ignore_errors: true # noqa ignore-errors

- name: OS is supported
  ansible.builtin.assert:
    that: __os_supported
    quiet: true
  vars:
    __os_supported: "{{ lookup('vars', '{}_os_supported'.format(role_name)) | bool }}"

- name: Include OS-specific tasks.
  ansible.builtin.include_tasks: "{{ ansible_os_family | lower }}.yml"

- name: Include DB tasks
  ansible.builtin.include_tasks: db.yml

- name: Deploy config
  ansible.builtin.template:
    src: homeserver.yaml.j2
    dest: /etc/matrix-synapse/homeserver.yaml
    owner: matrix-synapse
    group: matrix-synapse
    mode: "0600"
  notify: restart_matrix_synapse

- name: Deploy signing key
  ansible.builtin.template:
    src: homeserver.signing.key.j2
    dest: /etc/matrix-synapse/homeserver.signing.key
    owner: matrix-synapse
    group: matrix-synapse
    mode: "0600"
  no_log: true
  notify: restart_matrix_synapse

- name: Copy logging config
  ansible.builtin.template:
    src: log.yaml.j2
    dest: "{{ matrix_synapse_log_config }}"
    owner: matrix-synapse
    group: matrix-synapse
    mode: "0600"
  notify: restart_matrix_synapse

- name: copy config files
  ansible.builtin.template:
    src: "{{ item }}"
    dest: /etc/matrix-synapse/conf.d/{{ item | basename | regex_replace('\.j2$', '') }}
    owner: matrix-synapse
    group: matrix-synapse
    mode: "0600"
  with_fileglob:
    - "{{ role_path }}/templates/conf.d/*"
  notify: restart_matrix_synapse

- name: create nginx well-known directory
  ansible.builtin.file:
    path: "{{ matrix_synapse_wellknown_path }}"
    state: directory
    owner: root
    group: root
    mode: "0755"

- name: Deploy server well-known file
  ansible.builtin.copy:
    content: "{{ matrix_synapse_wellknown_server | to_json(indent=4, sort_keys=True) }}"
    dest: "{{ matrix_synapse_wellknown_path }}/server"
    owner: root
    group: root
    mode: "0644"

- name: Deploy client well-known file
  ansible.builtin.copy:
    content: "{{ matrix_synapse_wellknown_client | to_json(indent=4, sort_keys=True) }}"
    dest: "{{ matrix_synapse_wellknown_path }}/client"
    owner: root
    group: root
    mode: "0644"

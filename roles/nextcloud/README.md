## Role Variables

Role's variables (and their default values):

### Choose the version

An URL will be generated following naming rules used in the nextcloud repository
_Not following this rules correctly may make the role unable to download nextcloud._

#### Repository naming rules:
Some variables changes depending on the channel used and if get_latest is true.
This table summarize the possible cases.

|channel|latest|major&latest|major|full|special|
|---|---|---|---|---|---|
|**releases**|yes/no|_null_ \|9\|10\|...|_null_|"10.0.3"|_null_|
|**prereleases**|_null_|_null_|_null_|"11.0.1"|_null_ \|"RC(n)\|beta(n)"|
|**daily**|yes/no|_null_ \|master\|stable9\|...|master\|9\|10\|...|_null_|_null_ \|"YYYY-MM-DD"|

**major&latest** = major value when latest is true
_null_ = "not used"
#### version variables:
```YAML
nextcloud_version_channel: "releases" # releases | prereleases | daily
```
Specify the main channel to use.
```YAML
nextcloud_get_latest: true
```
Specify if the "latest" archive should be downloaded.

```YAML
# nextcloud_version_major: 10
```
Specify what major version you desire.

```YAML
# nextcloud_version_full: "10.0.3"
```
The full version of the desired nextcloud instance. type **M.F.P** _(Major.Feature.Patch)_

```YAML
# nextcloud_version_special: ""
```
Specify a special string in the archive's filename.
For prereleases: "RCn|beta" | for daily "YYYY-MM-DD"

```YAML
nextcloud_repository: "https://download.nextcloud.com/server"
```
Repository's URL.

```YAML
nextcloud_archive_format: "zip" # zip | tar.bz2
```
Choose between the 2 archive formats available in the repository.

```YAML
# nextcloud_full_url:
```
_If you don't like rules..._
Specify directly a full URL to the archive. The role will skip the url generation and download the archive. **Requires nextcloud_version_major to be set along**.
#### Examples:
- Download your own archive:
  (_you **must** specify the nextcloud major version along_)
```YAML
nextcloud_full_url: https://h2g2.com/42/nextcloud.zip
nextcloud_version_major: 42
```
-   Choose the latest release (default):
```YAML
nextcloud_version_channel: "releases"
nextcloud_get_latest: true
```
-   Choose the latest v10 release:
```YAML
nextcloud_version_channel: "releases"
nextcloud_get_latest: true
nextcloud_version_major: 10
```
-   Choose a specific release:
```YAML
nextcloud_version_channel: "releases"
nextcloud_get_latest: false
nextcloud_full_version: "10.0.3"
```
-   Get the nextcloud 11.0.1 prerelease 1:
```YAML
nextcloud_version_channel: "prereleases"
nextcloud_version_full: "11.0.1"
nextcloud_version_special: "RC1"
```
-   Get the latest daily:
```YAML
nextcloud_version_channel: "daily"
nextcloud_get_latest: true
```
-   Get the latest daily for stable 10:
```YAML
nextcloud_version_channel: "daily"
nextcloud_get_latest: true
nextcloud_version_major: "stable10"
```
-   Get the daily for master at january 1rst 2017:
```YAML
nextcloud_version_channel: "daily"
nextcloud_get_latest: false
nextcloud_version_major: "master"
nextcloud_version_special: "2017-01-01"
```
### Main configuration
```YAML
nextcloud_trusted_domain:
  - "{{ ansible_fqdn }}"
  - "{{ ansible_default_ipv4.address }}"
```
The list of domains you will use to access the same Nextcloud instance.
```YAML
nextcloud_trusted_proxies: []
```
The list of trusted proxies IPs if Nextcloud runs through a reverse proxy.
```YAML
nextcloud_instance_name: "{{ nextcloud_trusted_domain | first }}"
```
The name of the Nextcloud instance. By default, the first element in the list of trusted domains
### WebServer configuration
```YAML
nextcloud_install_websrv: true
```
The webserver setup can be skipped if you have one installed already.
```YAML
nextcloud_websrv: "apache2"
```
The http server used by nextcloud. Available values are: **apache2** or **nginx**.
```YAML
nextcloud_disable_websrv_default_site: false
```
Disable the default site of the chosen http server. (`000-default.conf` in Apache, `default` in Nginx.)
```YAML
nextcloud_websrv_template: "templates/{{nextcloud_websrv}}_nc.j2"
```
The jinja2 template creating the instance configuration for your webserver.
You can provide your own through this parameter.
```YAML
nextcloud_webroot: "/srv/nextcloud"
```
The Nextcloud root directory.
```YAML
nextcloud_data_dir: "/srv/nextcloud-data"
```
The Nextcloud data directory. This directory will contain all the Nextcloud files. Choose wisely.
```YAML
nextcloud_admin_name: "admin"
```
Defines the Nextcloud admin's login.
```YAML
nextcloud_admin_pwd: "secret"
```
Defines the Nextcloud admin's password.  
**Not defined by default**  
If not defined by the user, a random password will be generated.

```YAML
nextcloud_max_upload_size: "512m"
```
Defines the max size allowed to be uploaded on the server.  
Use 0 to __disable__.

### Redis Server configuration
```YAML
nextcloud_install_redis_server: true
```
Whenever the role should install a redis server on the same host.
```YAML
nextcloud_redis_host: '/var/run/redis/redis.sock'
```
The Hostname of redis server. It is set to use UNIX socket as redis is on same host. Set to hostname if it is not the case.
```YAML
nextcloud_redis_port: 0
```
The port of redis server. Port 0 is for socket use. Default redis port is 6379.
```YAML
nextcloud_redis_settings:
  - { name: 'redis host', value: '"{{ nextcloud_redis_host }}"' }
  - { name: 'redis port', value: "{{ nextcloud_redis_port }}" }
  - { name: 'memcache.locking', value: '\OC\Memcache\Redis' }
```
Settings to use redis server with Nextcloud

### Nextcloud Background Jobs
```YAML
nextcloud_background_cron: True
```
Set operating system cron for executing Nextcloud regular tasks. This method enables the execution of scheduled jobs without the inherent limitations the Web server might have.

### Custom nextcloud settings
```YAML
nextcloud_config_settings:
  - { name: 'overwrite.cli.url', value: 'https://{{ nextcloud_trusted_domain | first }}' }
  - { name: 'memcache.local', value: '\OC\Memcache\APCu' }
  - { name: 'open_basedir', value: '/dev/urandom' }
  - { name: 'mysql.utf8mb4', value: 'true' }
  - { name: 'updater.release.channel', value: 'production' } # production | stable | daily | beta
```
Setting custom Nextcloud setting in config.php ( [Config.php Parameters Documentations](https://docs.nextcloud.com/server/stable/admin_manual/) )

Default custom settings:
-   **Base URL**: 'https:// {{nextcloud_instance_name}}'
-   **Memcache local**: APCu
-   **Mysql Character Set**: utf8mb4
-   **PHP read access to /dev/urandom**: Enabled
-   **Updater Relese Channel:** Production
### Database configuration
```YAML
nextcloud_install_db: true
```
Whenever the role should install and configure a database on the same host.
```YAML
nextcloud_db_host: "127.0.0.1"
```
The database server's ip/hostname where Nextcloud's database is located.
```YAML
nextcloud_db_backend: "mysql"
```
Database type used by nextcloud.

Supported values are:
-   mysql
-   mariadb
-   pgsql _(PostgreSQL)_

```YAML
nextcloud_db_name: "nextcloud"
```
The Nextcloud instance's database name.
```YAML
nextcloud_db_admin: "ncadmin"
```
The Nextcloud instance's database user's login
```YAML
nextcloud_db_pwd: "secret"
```
The Nextcloud instance's database user's password.

**Not defined by default.**

If not defined by the user, a random password will be generated.

### TLS configuration
```YAML
nextcloud_install_tls: true
```
TLS setup can be skipped if you manage it separately (e.g. behind a reverse proxy).
```YAML
nextcloud_tls_enforce: true
```
Force http to https.
```YAML
nextcloud_mozilla_modern_ssl_profile: true
```
Force Mozilla modern SSL profile in webserver configuration (intermediate profile is used when false).
```YAML
nextcloud_hsts: false
```
Set HTTP Strict-Transport-Security header (e.g. "max-age=15768000; includeSubDomains; preload").

_(Before enabling HSTS, please read into this topic first)_
```YAML
nextcloud_tls_cert_method: "self-signed"
```
Defines various method for retrieving a TLS certificate.
-   **self-signed**: generate a _one year_ self-signed certificate for the trusted domain on the remote host and store it in _/etc/ssl_.
-   **signed**: copy provided signed certificate for the trusted domain to the remote host or in /etc/ssl by default.
  Uses:
```YAML
  # Mandatory:
  nextcloud_tls_src_cert: /local/path/to/cert
  # ^local path to the certificate's key.
  nextcloud_tls_src_cert_key: /local/path/to/cert/key
  # ^local path to the certificate.

  # Optional:
  nextcloud_tls_cert: "/etc/ssl/{{ nextcloud_trusted_domain }}.crt"
  # ^remote absolute path to the certificate's key.
  nextcloud_tls_cert_key: "/etc/ssl/{{ nextcloud_trusted_domain }}.key"
  # ^remote absolute path to the certificate.
```
-   **installed**: if the certificate for the trusted domain is already on the remote host, specify its location.
  Uses:
```YAML
  nextcloud_tls_cert: /path/to/cert
  # ^remote absolute path to the certificate's key. mandatory
  nextcloud_tls_cert_key: /path/to/cert/key
  # ^remote absolute path to the certificate. mandatory
  nextcloud_tls_cert_chain: /path/to/cert/chain
  # ^remote absolute path to the certificate's full chain- used only by apache - Optional
```

### System configuration

install and use a custom version for PHP instead of the default one:
```YAML
php_ver: '7.1'
php_custom: yes
php_ver: "{{ php_ver }}"
php_dir: "/etc/php/{{ php_ver }}"
php_bin: "php-fpm{{ php_ver }}"
php_pkg_apcu: "php-apcu"
php_pkg_spe:
  - "php{{ php_ver }}-imap"
  - "php{{ php_ver }}-imagick"
  - "php{{ php_ver }}-xml"
  - "php{{ php_ver }}-zip"
  - "php{{ php_ver }}-mbstring"
  - "php-redis"
php_socket: "/run/php/{{ php_ver }}-fpm.sock"
```

```YAML
nextcloud_websrv_user: "www-data"
```
system user for the http server
```YAML
nextcloud_websrv_group: "www-data"
```
system group for the http server
```YAML
nextcloud_mysql_root_pwd: "secret"
```
root password for the mysql server

**Not defined by default**

If not defined by the user, and mysql/mariadb is installed during the run, a random password will be generated.

### Generated password
The role uses Ansible's password Lookup:
-   If a password is generated by the role, ansible stores it **locally** in **nextcloud_instances/{{ nextcloud_trusted_domain }}/** (relative to the working directory)
-   if the file already exist, it reuse its content
-   see [the ansible password lookup documentation](https://docs.ansible.com/ansible/latest/plugins/lookup/password.html) for more info

### Post installation:
#### Applications installation

Since **v1.3.0**, it is possible to download, install and enable nextcloud applications during a post-install process.

The application (app) to install have to be declared in the `nextcloud_apps` dictionary in a "key:value" pair.
-   The app name is the key
-   The download link, is the value.

```YAML
nextcloud_apps:
  app_name_1: "http://download_link.com/some_archive.zip"
  app_name_2: "http://getlink.com/another_archive.zip"
```

Alternatively, if you need to configure an application after enabling it, you can use this structure.
```YAML
nextcloud_apps:
  app_name_1:
    source: "http://download_link.com/some_archive.zip"
    conf:
      parameter1: ldap:\/\/ldapsrv
      parameter2: another_value
```

**Notes:**
-   Because the role is using nextcloud's occ, it is not possible to install an app from the official nextcloud app store.
-   If you know that the app is already installed, you can give an empty string to skip the download.
-   The app name need the be equal to the folder name located in the **apps folder** of the nextcloud instance, which is extracted from the downloaded archive.
The name may not be canon some times. (like **appName-x.y.z** instead of **appName**)
-   The role will **not** update an already enabled application.
-   The configuration is applied only when the app in enabled the first time:
Changing a parameter, then running the role again while the app is already enabled will **not** update its configuration.
-   this post_install process is tagged and can be called directly using the `--tags install_apps` option.

## License

BSD

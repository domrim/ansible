---
# vars file for nextcloud
nextcloud_dl_file_name:
  latest: "{{ ['latest', nextcloud_version_major] | reject('undefined') | join('-') }}"
  releases: "{{ ['nextcloud', nextcloud_version_full] | reject('undefined') | join('-') }}"
  prereleases: nextcloud-{{ [nextcloud_version_full, nextcloud_version_special] | reject('undefined') | join() }}
  daily: nextcloud-{{ nextcloud_version_major | d('') }}-daily-{{ nextcloud_version_special | d('') }}

nextcloud_calculated_file: "{{ nextcloud_dl_file_name[nextcloud_ver_dict_key] }}.tar.bz2"
nextcloud_ver_dict_key: "{{ 'latest' if ((nextcloud_get_latest | bool) and (nextcloud_version_channel != 'prereleases')) else nextcloud_version_channel }}"

nextcloud_release_channels:
  releases: stable
  prereleases: beta
  daily: daily
nextcloud_release_channel: "{{ nextcloud_release_channels[nextcloud_version_channel] }}"

nextcloud_max_upload_size_in_bytes: "{{ nextcloud_max_upload_size | human_to_bytes }}"

nextcloud_config_base:
  system:
    trusted_domains: "{{ nextcloud_domains }}"
    redis:
      host: "{{ nextcloud_redis_host }}"
      port: "{{ nextcloud_redis_port }}"
      dbindex: "{{ nextcloud_redis_dbindex }}"
    memcache.local: \OC\Memcache\APCu
    memcache.distributed: \OC\Memcache\Redis
    memcache.locking: \OC\Memcache\Redis
    overwrite.cli.url: https://{{ nextcloud_domains | first }}
    htaccess.RewriteBase: /
    default_phone_region: DE
    filelocking.enabled: true
    updater.release.channel: "{{ nextcloud_release_channel }}"
    skeletondirectory: ""
    loglevel: 2
    defaultapp: files
    trashbin_retention_obligation: auto, 365
    maintenance_window_start: 1
    mail_smtpmode: smtp
    mail_sendmailmode: smtp
    mail_from_address: no-reply
    mail_domain: "{{ nextcloud_domains | first }}"
    mail_smtphost: localhost
    mail_smtpport: "25"
  apps:
    core:
      backgroundjobs_mode: "{{ 'cron' if nextcloud_cron_systemd else 'ajax' }}"

nextcloud_config_dict: "{{ nextcloud_config_base | combine(nextcloud_config_custom | default({}), recursive=True) }}"

nextcloud_core_config:
  - regex: "'datadirectory'"
    line: "  'datadirectory' => '{{ nextcloud_data_dir }}'"
  - regex: "'dbtype'"
    line: "  'dbtype' => '{{ nextcloud_db_backend }}'"
  - regex: "'dbhost'"
    line: "  'dbhost' => '{{ nextcloud_db_host }}'"
  - regex: "'dbport'"
    line: "  'dbport' => '{{ nextcloud_db_port }}'"
  - regex: "'dbname'"
    line: "  'dbname' => '{{ nextcloud_db_name }}'"
  - regex: "'dbuser'"
    line: "  'dbuser' => '{{ nextcloud_db_user }}'"
  - regex: "'dbpassword'"
    line: "  'dbpassword' => '{{ nextcloud_db_password }}'"

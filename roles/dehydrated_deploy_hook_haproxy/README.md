# Ansible Role: dehydrated_deploy_hook_haproxy

This role configures a dehydrated deploy hook for haproxy

## Dependencies

- scc_net.web.dehydrated

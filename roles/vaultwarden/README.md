# Vaultwarden

####  This role was previously known as `dmaes.bitwardenrs` and `dmaes.vaultwarden`

Since the Bitwarden_RS project changed names to Vaultwarden, so did this role (see #12 for more info).
Force this change, we changed everything from `bitwardenrs` to `vaultwarden` (variables used in the ansible code, but also directories, user, systemd service, etc.)
When making the switch:
* Stop old `bitwarden_rs` service
* Make a backup of both files and database for good measure
* Update your ansible code to work with new role
* Either point `vaultwarden_directory` to the old directory, or move stuff to the new default (`/opt/vaultwarden`). Also pay attention to `vaultwarden_datadir` if using a custom one.
* The new vaultwarden user should get the same rights on the database as your previous bitwardenrs user
  * for postgres:
     * su - postgres
     * psql
     * `postgres-# GRANT bitwardenrs TO vaultwarden;`
* Run ansible, this will create everything under the new name (user and service, not directory)
* Cleanup old user, service (and possibly (data)directory)


[![Build Status](https://travis-ci.com/dmaes/ansible-role-vaultwarden.svg?branch=master)](https://travis-ci.com/dmaes/ansible-role-vaultwarden)

Builds, installs and configures [Vaultwarden](https://github.com/dani-garcia/vaultwarden) (without Docker).

*Only tested on Debian 10 and CentOS 8*

## Requirements
* Requirements for the [unarchive](https://docs.ansible.com/ansible/latest/modules/unarchive_module.html)-module
* Requirements for the [package](https://docs.ansible.com/ansible/latest/modules/package_module.html)-module
* wget or curl
* jinja => v2.11
* Systemd (optional)

## Role Variables
| Variable | Description | Default value |
| --- | --- | --- |
| `vaultwarden_directory` | Where to install Vaultwarden | `/srv/vaultwarden` |
| `vaultwarden_version` | Which version to install | `1.17.0` |
| `vaultwarden_webvault` | Install the patched webvault | `true` |
| `vaultwarden_webvault_version` | Version of the webvault to install | `2.16.1` |
| `vaultwarden_force_recompile` | Force recompile binary, (e.g. you switched backends on same server | `false` |
| `vaultwarden_config` | Key-value environment variables for the Vaultwarden `.env` file | `{ DOMAIN: "https://{{ ansible_fqdn }}/" }` |
| `vaultwarden_datadir` | Vaultwarden data directory (does not configure, only create and used for e.g. keys) | `{{ vaultwarden_directory }}/data` |
| `vaultwarden_encryption_key` | RSA key to use for encryption (content, not file), empty string to not copy, Vaultwarden should generate one if non-existing | `""` |
| `vaultwarden_force_encryption_key` | Force changing encryption key if it already exists (DANGEROUS!) | `false` |

There exist more undocumented config-vars ;)

## Example Playbook
```yaml
- hosts: servers
  vars:
    admin_token: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      ...
    vaultwarden_domain: "{{ ansible_fqdn }}"
    vaultwarden_database_url: "postgresql:///{{ vaultwarden_db_name }}?host=/run/postgresql/"
    vaultwarden_admin_token: "{{ lookup('fsetit.general.gopass', '{}/vaultwarden/admin_token'.format(ansible_fqdn), create=True, password_length=48, use_symbols=False) }}"
    vaultwarden_websocket_enabled: True
    vaultwarden_signups_allowed: False
    vaultwarden_signups_verify: True
    vaultwarden_signups_domains_allowlist:
      - "{{ ansible_domain }}"
    vaultwarden_log_level: "warn"
    vaultwarden_smtp_host: "localhost"
    vaultwarden_smtp_port: 25
    vaultwarden_smtp_ssl: False
    vaultwarden_smtp_from: "no-reply@{{ vaultwarden_domain }}"
    vaultwarden_smtp_from_name: "Vaultwarden"
    vaultwarden_custom_config:
      DOMAIN: "https://example.com/"
      DOMAIN_PATH: "/vaultwarden"  # results in a domain of https://example.com/vaultwarden/, needs to start with a '/'
      ADMIN_TOKEN: "{{ admin_token }}"
      SIGNUPS_ALLOWED: 'false'
      SIGNUPS_VERIFY: 'true'
      SIGNUPS_DOMAINS_WHITELIST: 'example.com'
      INVITATIONS_ALLOWED: 'true'
      SMTP_HOST: 'mail.example.com'
      SMTP_FROM: 'vaultwarden@example.com'
      SMTP_FROM_NAME: 'vaultwarden'
  roles:
    - vaultwarden
```

## License
MIT

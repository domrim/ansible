---
- name: Set OS dependent variables
  ansible.builtin.include_vars: "{{ lookup('first_found', params) }}"
  vars:
    params:
      files:
        - "{{ ansible_distribution | lower }}_{{ ansible_distribution_version | lower }}.yml"
        - "{{ ansible_distribution | lower }}_{{ ansible_distribution_major_version | lower }}.yml"
        - "{{ ansible_distribution | lower }}.yml"
        - "{{ ansible_os_family | lower }}.yml"
        - "{{ ansible_system | lower }}.yml"
      paths:
        - "{{ role_path }}/vars"
  ignore_errors: true # noqa ignore-errors

- name: OS is supported
  ansible.builtin.assert:
    that: __os_supported
    quiet: true
  vars:
    __os_supported: "{{ lookup('vars', '{}_os_supported'.format(role_name)) | bool }}"

- name: create user
  ansible.builtin.user:
    name: "{{ maubot_user }}"
    home: "{{ maubot_user_home }}"
    shell: /bin/bash
    state: present
  register: maubot_user_reg

- name: Install package requirements
  ansible.builtin.apt:
    name:
      # python dependencies
      - python3-setuptools
      - python3-pip
      - python3-venv
      - virtualenv
      # matrix olm (e2ee) dependencies
      - libolm-dev
      - python3-dev
      - build-essential
    state: present

- name: get github release
  ansible.builtin.include_role:
    role: domrim.general.github_release
  vars:
    github_release_role_name: maubot
    github_release_project_path: maubot/maubot
    github_release_prefix: v
  when: maubot_version is not defined

- name: set version to install. either hardcoded or latest release.
  ansible.builtin.set_fact:
    __maubot_version: "{{ maubot_version | default(__maubot_github_release_version) }}"

- name: install maubot packages
  become: true
  become_user: "{{ maubot_user }}"
  ansible.builtin.pip:
    name: "{{ maubot_python_packages }}"
    state: present
    virtualenv: "{{ maubot_venv_directory }}"
    virtualenv_python: python3
    extra_args: --upgrade --upgrade-strategy=eager
  notify: restart maubot

- name: install required packages for plugins
  become: true
  become_user: "{{ maubot_user }}"
  ansible.builtin.pip:
    name: "{{ maubot_plugin_python_packages }}"
    state: present
    virtualenv: "{{ maubot_venv_directory }}"
    virtualenv_python: python3
    extra_args: --upgrade --upgrade-strategy=eager
  notify: restart maubot

- name: Create data directory structure
  ansible.builtin.file:
    path: "{{ maubot_data_dir }}/{{ item }}"
    state: directory
    mode: "0750"
    owner: "{{ maubot_user_reg.uid }}"
    group: "{{ maubot_user_reg.group }}"
  loop: "{{ ([maubot_plugin_upload_dir, maubot_plugin_trash_dir] + maubot_plugin_load_dirs) | unique }}"

- name: Template maubot config
  ansible.builtin.template:
    src: config.yaml.j2
    dest: "{{ maubot_data_dir }}/config.yaml"
    mode: "0600"
    owner: "{{ maubot_user_reg.uid }}"
    group: "{{ maubot_user_reg.group }}"
  notify: restart maubot

- name: Setup systemd service
  ansible.builtin.template:
    src: systemd.service.j2
    dest: /etc/systemd/system/maubot.service
  notify:
    - daemon_reload
    - restart maubot

- name: Enable systemd service
  ansible.builtin.systemd:
    name: maubot.service
    daemon_reload: true
    enabled: true

#!/usr/bin/env python3
# -*- coding: utf-8 -*

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import base64
import random
import secrets
import string
import sys

from ansible.errors import AnsibleError, AnsibleFilterError
from ansible.module_utils.common.text.converters import to_bytes, to_native, to_text
from ansible.module_utils.six import reraise
from ansible.utils.display import Display
from ansible.utils.encrypt import PasslibHash, BaseHash
from hashlib import pbkdf2_hmac
from collections import namedtuple


display = Display()


class Argon2PasslibHash(PasslibHash, BaseHash):
    argon2_algo = namedtuple('algo', ['argon_type', 'time_cost', 'memory_cost', 'parallelism'])
    algorithms = {
        'argon2': argon2_algo(argon_type='id', time_cost=3, memory_cost=4096, parallelism=1),
    }

    def __init__(self, algorithm):
        super(Argon2PasslibHash, self).__init__(algorithm=algorithm)

    def hash(self, secret, salt=None, salt_size=None, argon_type=None, time_cost=None, memory_cost=None, parallelism=None):
        salt = self._clean_salt(salt)
        argon_type = self._clean_setting('argon_type', argon_type)
        time_cost = self._clean_setting('time_cost', time_cost)
        memory_cost = self._clean_setting('memory_cost', memory_cost)
        parallelism = self._clean_setting('parallelism', parallelism)
        return self._hash(secret, salt=salt, salt_size=salt_size, argon_type=argon_type, time_cost=time_cost, memory_cost=memory_cost, parallelism=parallelism)

    def _clean_setting(self, name, value):
        algo_data = self.algorithms.get(self.algorithm)
        if value:
            return value
        else:
            return getattr(algo_data, name)


    def _hash(self, secret, salt, salt_size, argon_type, time_cost, memory_cost, parallelism):
        # Not every hash algorithm supports every parameter.
        # Thus create the settings dict only with set parameters.
        settings = {}
        if salt:
            settings['salt'] = salt
        if salt_size:
            settings['salt_size'] = salt_size
        if argon_type:
            settings['type'] = argon_type
        if time_cost:
            settings['time_cost'] = time_cost
        if memory_cost:
            settings['memory_cost'] = memory_cost
        if parallelism:
            settings['parallelism'] = parallelism

        # starting with passlib 1.7 'using' and 'hash' should be used instead of 'encrypt'
        try:
            result = self.crypt_algo.using(**settings).hash(secret)
        except ValueError as e:
            raise AnsibleError("Could not hash the secret.", orig_exc=e)

        # passlib.hash should always return something or raise an exception.
        # Still ensure that there is always a result.
        # Otherwise an empty password might be assumed by some modules, like the user module.
        if not result:
            raise AnsibleError("failed to hash with algorithm '%s'" % self.algorithm)

        # Hashes from passlib.hash should be represented as ascii strings of hex
        # digits so this should not traceback.  If it's not representable as such
        # we need to traceback and then block such algorithms because it may
        # impact calling code.
        return to_text(result, errors='strict')


def b64urlencode(string: str, encoding: str = "utf-8") -> str:
    return to_text(base64.urlsafe_b64encode(to_bytes(string, encoding=encoding, errors='surrogate_or_strict'))).rstrip("=")


def string_salt(seed: str, length: int = 16):
    random.seed(seed)
    res_list = random.choices(string.ascii_letters + string.digits, k=length)
    return to_text(''.join(res_list))


def bcrypt_salt(seed):
    random.seed(seed)
    res_list = random.choices('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890./', k=21) + random.choices('.Oeu', k=1)
    return to_text(''.join(res_list))


def hexencode(s: str):
    hex_list = []
    for char in s:
        hex_representation = char.encode('utf-8').hex()
        hex_list.append(f'&#x{hex_representation};')

    return ''.join(hex_list)


def get_encrypted_password_pbkdf2(password: str, salt: str, hashtype: str = 'sha512', rounds: int = 29000):
    dk = pbkdf2_hmac(hash_name=hashtype, password=password.encode('utf8'), salt=base64.b64decode(salt.encode('utf-8')), iterations=rounds)

    r_dk = base64.b64encode(dk).decode('utf8')
    return f'$7${rounds}${salt}${r_dk}'


def get_encrypted_password_argon2(password, hashtype='argon2', salt=None, salt_size=None, argon_type=None, time_cost=None, memory_cost=None, parallelism=None):
    passlib_mapping = {
        'argon2': 'argon2',
    }
    hashtype = passlib_mapping.get(hashtype, hashtype)

    if hashtype not in passlib_mapping and hashtype not in passlib_mapping.values():
        choices = ', '.join(passlib_mapping)
        raise AnsibleFilterError(f"{hashtype} is not in the list of supported passlib algorithms: {choices}")

    try:
        return Argon2PasslibHash(hashtype).hash(password, salt=salt, salt_size=salt_size, argon_type=argon_type, time_cost=time_cost, memory_cost=memory_cost, parallelism=parallelism)
    except AnsibleError as e:
        reraise(AnsibleFilterError, AnsibleFilterError(to_native(e), orig_exc=e), sys.exc_info()[2])


class FilterModule(object):
    """ Ansible custom jinja2 filters """

    def filters(self):
        return {
            "b64urlencode": b64urlencode,
            "bcrypt_salt": bcrypt_salt,
            "hexencode": hexencode,
            "password_hash_argon2": get_encrypted_password_argon2,
            "password_hash_pbkdf2": get_encrypted_password_pbkdf2,
            "string_salt": string_salt,
        }
